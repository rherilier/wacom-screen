#!/bin/sh

# Copyright © 2023 Rémi Hérilier
# This file is part of the wacom-screen projet
# SPDX-License-Identifier: WTFPL

##############################################################################
# dependencies check
#
which xrandr > /dev/null || { echo "missing command: xrandr" ; exit 1 ; }
which xsetwacom > /dev/null || { echo "missing command: xsetwacom" ; exit 1 ; }
which bc > /dev/null || { echo "missing command: bc" ; exit 1 ; }

##############################################################################
# parameters check
#
test $# -eq 0 -o $# -gt 2 && echo "usage: `basename $0` [set (screen-name)|reset]" && exit 1

CMD="$1"
shift

case "$CMD" in
	set)
		if test -n "$1"
		then
			xrandr | grep -w connected | awk '{print $1}' | grep -q -x "$1"
			test $? -ne 0 && echo "unknown/disconnected screen '$1'" && exit 1

			SCREEN_NAME=$1
			shift
		fi

		test $# != 0 && echo "extra parameter(s): '$1'" && exit 1
		;;
	reset)
		test $# != 0 && echo "extra parameter(s): '$1'" && exit 1
		;;
	*)
		echo "unknown command: $CMD"
		exit 1
		;;
esac

##############################################################################
# $1: device ID
#
wacom_test_default_area()
{
	xsetwacom --get $1 Area | while read A B W H
	do
		test $A = 0 -a $B = 0
	done
}

##############################################################################
# $1: device ID
#
wacom_set_area()
{
	xsetwacom --get $1 Area | while read A B W H
	do
		WACOM_RATIO=`echo "$W / $H" | bc -l`

		if test $WACOM_RATIO != $SCREEN_RATIO
		then
			CX=`echo "$W / 200" | bc -l`
			CY=`echo "$H / 200" | bc -l`
			RES=`echo "$WACOM_RATIO < $SCREEN_RATIO" | bc`

			if test $RES -eq  1
			then
				HW=$CX
				HH=`echo "($CX / $SCREEN_RATIO)" | bc -l`
			else
				HW==`echo "($CY * $SCREEN_RATIO)" | bc -l`
				HH=$CY
			fi

			X0=`echo "((($CX - $HW) * 100) + 0.5) / 1" | bc`
			X1=`echo "((($CX + $HW) * 100) + 0.5) / 1" | bc`
			Y0=`echo "((($CY - $HH) * 100) + 0.5) / 1" | bc`
			Y1=`echo "((($CY + $HH) * 100) + 0.5) / 1" | bc`

			xsetwacom set $1 Area "$X0 $Y0 $X1 $Y1"
		fi
	done
}

##############################################################################
# $1: device ID
#
wacom_print_area()
{
	xsetwacom --get $1 Area | { read X0 Y0 X1 Y1 ; echo "device $1 set to \"$X0 $Y0 $X1 $Y1\"" ; }
}

##############################################################################
# main
#
WACOM_IDS=`xsetwacom --list devices | sed -e 's/^.*id://' | awk '{print $1}'`

case $CMD in
	set)
		SCREEN_STR=`xrandr | grep -w connected | head -n 1`
		SCREEN_SIZE=`echo $SCREEN_STR | sed -e 's/^.*) //' -e 's/mm//g'`

		SCREEN_WIDTH=`echo $SCREEN_SIZE | cut -d\  -f 1`
		SCREEN_HEIGHT=`echo $SCREEN_SIZE | cut -d\  -f 3`
		SCREEN_RATIO=`echo "$SCREEN_WIDTH / $SCREEN_HEIGHT" | bc -l`

		if test -z "$SCREEN_NAME"
		then
			SCREEN_NAME=`echo $SCREEN_STR | awk '{print $1}'`
		fi

		for ID in $WACOM_IDS
		do
			if wacom_test_default_area $ID
			then
				xsetwacom set $ID MapToOutput $SCREEN_NAME
				wacom_set_area $ID
				wacom_print_area $ID
			else
				echo "device $ID already set"
			fi
		done
		;;
	reset)
		for ID in $WACOM_IDS
		do
			if wacom_test_default_area $ID
			then
				echo "device $ID already reset"
			else
				xsetwacom set $ID MapToOutput desktop
				xsetwacom set $ID ResetArea
				wacom_print_area $ID
			fi
		done
		;;
esac
