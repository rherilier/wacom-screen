# About

This program aims to simply restraint a tablet's pen to a given screen (or to the whole desktop).

It is mainly a rewrite in C language of my old Bourne shell script `old/wacom-screen.sh` (for the record).

Ho! I forget: the tablet effective area keeps the aspect-ratio of the targeted screen.

It is also to play with X11 internals and bash completion too.

# License

The shell script old/wacom-screen.sh` remains under its original license; the Do What the Fuck You Want to Public License (see old/WTFPL for details).

the other files are distributed under the GNU General Public License version 3 or later (see GPL-3 for details).

# Documentation

```shell
$ wacom-screen help
```

## Requirements

* CMake 3.6 or above;
* a C99 compiler (only GCC and Clang are supported).

## Contribute

1. Fork;
2. Write code;
3. Send a merge request.
