/* Copyright © 2023 Rémi Hérilier
 * This file is part of the wacom-screen projet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INPUTS_H
#define INPUTS_H

#include <X11/Xlib.h>

/* describes a tablet
 *
 * @note area's values are expressed in hundreth of millimeters
 */
struct input_t
{
    char* name;
    char* type;
    char* dev;
    XID id;
    long current_area[4];
    long default_area[4];
    int is_landscape;
};

/* initialize internal inputs list
 *
 * @param dpy X11 display
 */
int inputs_init(Display* dpy);

/* find an input given its name
 *
 * @param name the wanted name
 *
 * @return a pointer on the corresponding input if found; NULL othewise
 */
const struct input_t* inputs_exists(const char* name);

/* print on stdout the inputs' names and their related informations
 */
void inputs_print_details();

/* print on stdout the inputs' names
 */
void inputs_print_names();

/* set the input device working area respecting an aspect ratio
 *
 * @param input the target input
 * @param area 4 integers for the upper-left and lower-right corners
 * @param aspect_ratio the wanted proportion
 */
void inputs_set_area(Display* dpy, const struct input_t* input, const float aspect_ratio);

/* reset the input device working area to its default value
 *
 * @param input the target input
 */
void inputs_reset_area(Display* dpy, const struct input_t* input);

/* set the input device effective screen area to a given geometry
 *
 * @param input the target input
 * @param x the area's x
 * @param y the area's y
 * @param w the area's width
 * @param h the area's height
 */
void inputs_set_viewport(Display* dpy, const struct input_t* input, int x, int y, int w, int h);

#endif /* INPUTS_H */
