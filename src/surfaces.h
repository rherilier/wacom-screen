/* Copyright © 2023 Rémi Hérilier
 * This file is part of the wacom-screen projet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef SURFACES_H
#define SURFACES_H

#include <X11/Xlib.h>

#define SURFACE_DESKTOP "Desktop"

struct surface_t
{
    char* name;
    int x;
    int y;
    int width;
    int height;
    int mmwidth;
    int mmheight;
    int is_desktop;
    int is_landscape;
};

/* initialize internal surfaces list
 *
 * @param dpy X11 display
 */
int surfaces_init(Display* dpy);

/* find a surface given its name
 *
 * @param name the wanted name
 *
 * @return a pointer on the corresponding surface if found; NULL othewise
 */
const struct surface_t* surfaces_exists(const char* name);

/* print on stdout the surfaces' names and their related informations
 */
void surfaces_print_details();

/* print on stdout the surfaces' names
 */
void surfaces_print_names();

#endif /* SURFACES_H */
