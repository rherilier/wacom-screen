/* Copyright © 2023 Rémi Hérilier
 * This file is part of the wacom-screen projet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "inputs.h"
#include "surfaces.h"

#include <libgen.h>
#include <string.h>
#include <stdio.h>

static int print_surfaces_names(Display* dpy, int argc, char** argv);
static int print_surfaces_details(Display* dpy, int argc, char** argv);

static int print_inputs_names(Display* dpy, int argc, char** argv);
static int print_inputs_details(Display* dpy, int argc, char** argv);

static int pin_input(Display* dpy, int argc, char** argv);
static int unpin_input(Display* dpy, int argc, char** argv);

static int print_help(Display* dpy, int argc, char** argv);

struct command_t {
    const char* name;
    const char* args;
    const char* desc;
    int(*fn)(Display*, int, char**);
};

static struct command_t commands[] = {
    {
        "help",
        NULL,
        " print this help.",
        print_help
    },
    { "show-surfaces",
      NULL,
      "print any relevant areas' names and their geometry.",
      print_surfaces_details
    },
    {
        "list-surfaces",
        NULL,
        "print relevant surfaces' names.",
        print_surfaces_names
    },
    { "show-inputs",
      NULL,
      "print any relevant inputs and their informations.",
      print_inputs_details
    },
    { "list-inputs",
      NULL,
      "print relevant inputs' names.",
      print_inputs_names
    },
    {
        "pin",
        "input-name surface-name",
        "pin the device input-name to the surface-name's area.",
        pin_input
    },
    {
        "unpin",
        "input-name",
        "reset the device input-name to the whole desktop's area.",
        unpin_input
    },
};

int ncommands = sizeof(commands) / sizeof(struct command_t);

Display* dpy = NULL;

static int search_command(const char* cmd)
{
    for (int i = 0; i < ncommands; ++i) {
        if (strcmp(cmd, commands[i].name) == 0) {
            return (int)i;
        }
    }

    return -1;
}

static void help(char* prog_name)
{
    const char* bname = basename(prog_name);

    printf("usage:\n");

    for (int i = 0; i < ncommands; ++i) {
        printf("  %s ", bname);

        if (commands[i].args) {
            printf("%s %s\n", commands[i].name, commands[i].args);
        } else {
            printf("%s\n", commands[i].name);
        }

        printf("    %s\n", commands[i].desc);
    }
}

int main(int argc, char** argv)
{
    if (argc == 1) {
        help(argv[0]);

        return 0;
    }

    dpy = XOpenDisplay(NULL);

    surfaces_init(dpy);
    inputs_init(dpy);

    int i = search_command(argv[1]);
    int ret = 1;

    if (i < 0) {
        fprintf(stderr, "EE: unknown command '%s'\n", argv[1]);
    } else {
        ret = commands[i].fn(dpy, argc, argv);
    }

    XCloseDisplay(dpy);

    return ret;
}

static int print_surfaces_names(Display* dpy, int argc, char** argv)
{
    (void)dpy;
    (void)argc;
    (void)argv;

    surfaces_print_names();

    return 0;
}

static int print_surfaces_details(Display* dpy, int argc, char** argv)
{
    (void)dpy;
    (void)argc;
    (void)argv;

    surfaces_print_details();

    return 0;
}

static int print_inputs_names(Display* dpy, int argc, char** argv)
{
    (void)dpy;
    (void)argc;
    (void)argv;

    inputs_print_names();

    return 0;
}

static int print_inputs_details(Display* dpy, int argc, char** argv)
{
    (void)dpy;
    (void)argc;
    (void)argv;

    inputs_print_details();

    return 0;
}

static int pin_input(Display* dpy, int argc, char** argv)
{
    if (argc != 4) {
        fprintf(stderr, "EE: wrong arguments count\n");

        return -1;
    }

    const struct input_t* input = inputs_exists(argv[2]);

    if (input == NULL) {
        fprintf(stderr, "EE: invalid input-name\n");

        return -1;
    }

    const struct surface_t* surface = surfaces_exists(argv[3]);

    if (surface == NULL) {
        fprintf(stderr, "EE: invalid surface-name\n");

        return -1;
    }

    inputs_set_viewport(dpy, input, surface->x, surface->y, surface->width, surface->height);

    float ar = 1.0;

    if (surface->is_desktop) {
        ar = 1.0;
    } else if (surface->is_landscape == input->is_landscape) {
        ar = surface->mmwidth / (float)surface->mmheight;
    } else {
        ar = surface->mmheight / (float)surface->mmwidth;
    }

    inputs_set_area(dpy, input, ar);

    return 0;
}

static int unpin_input(Display* dpy, int argc, char** argv)
{
    if (argc != 3) {
        fprintf(stderr, "wrong arguments count\n");

        return -1;
    }

    const struct input_t* input = inputs_exists(argv[2]);

    if (input == NULL) {
        fprintf(stderr, "invalid input-name\n");

        return -1;
    }

    const struct surface_t* desktop = surfaces_exists(SURFACE_DESKTOP);

    inputs_set_viewport(dpy, input, desktop->x, desktop->y, desktop->width, desktop->height);
    inputs_reset_area(dpy, input);

    return 0;
}

static int print_help(Display* dpy, int argc, char** argv)
{
    (void)dpy;
    (void)argc;
    (void)argv;

    help(argv[0]);

    return 0;
}
