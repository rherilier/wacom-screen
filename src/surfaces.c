/* Copyright © 2023 Rémi Hérilier
 * This file is part of the wacom-screen projet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "surfaces.h"

#include <X11/extensions/Xrandr.h>

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

static int nsurfaces = 0;
static struct surface_t* surfaces = NULL;

int surfaces_init(Display* dpy)
{
    XRRScreenResources *res = XRRGetScreenResources(dpy, DefaultRootWindow(dpy));
    int isurfaces = 0;

    if (res == NULL) {
        return ENODATA;
    }

    for (int i = 0; i < res->noutput; ++i) {
        XRROutputInfo *output_info = XRRGetOutputInfo(dpy, res, res->outputs[i]);

        if ((output_info->crtc == 0) || (output_info->connection != RR_Connected)) {
            /* ignoring non-screen or disconnected ones */
            continue;
        }

        ++nsurfaces;

        XRRFreeOutputInfo(output_info);
    }

    ++nsurfaces;

    surfaces = (struct surface_t*)malloc(sizeof(struct surface_t) * nsurfaces);

    surfaces[0].name = strdup(SURFACE_DESKTOP);
    surfaces[0].x = 0;
    surfaces[0].y = 0;
    surfaces[0].width = DisplayWidth(dpy, DefaultScreen(dpy));
    surfaces[0].height = DisplayHeight(dpy, DefaultScreen(dpy));
    surfaces[0].mmwidth = -1;
    surfaces[0].mmheight = -1;
    surfaces[0].is_desktop = 1;
    surfaces[0].is_landscape = 1;

    ++isurfaces;

    for (int i = 0; i < res->noutput; ++i) {
        XRROutputInfo *output_info = XRRGetOutputInfo(dpy, res, res->outputs[i]);
        XRRCrtcInfo *crtc_info;

        if ((output_info->crtc == 0) || (output_info->connection != RR_Connected)) {
            /* ignoring non-screen or disconnected ones */
            continue;
        }

        crtc_info = XRRGetCrtcInfo(dpy, res, output_info->crtc);

        int rotation = crtc_info->rotation;

        surfaces[isurfaces].name = strdup(output_info->name);
        surfaces[isurfaces].x = crtc_info->x;
        surfaces[isurfaces].y = crtc_info->y;
        surfaces[isurfaces].width = crtc_info->width;
        surfaces[isurfaces].height = crtc_info->height;
        surfaces[isurfaces].is_desktop = 0;
        surfaces[isurfaces].is_landscape = (rotation == RR_Rotate_0) || (rotation == RR_Rotate_180);
        surfaces[isurfaces].mmwidth = output_info->mm_width;
        surfaces[isurfaces].mmheight = output_info->mm_height;

        XRRFreeCrtcInfo(crtc_info);
        XRRFreeOutputInfo(output_info);

        ++isurfaces;
    }

    XRRFreeScreenResources(res);

    return isurfaces;
}

const struct surface_t* surfaces_exists(const char* name)
{
    for (int i = 0; i < nsurfaces; ++i) {
        if (strcmp(name, surfaces[i].name) == 0) {
            return surfaces + i;
        }
    }

    return NULL;
}

void surfaces_print_details()
{
    for (int i = 0; i < nsurfaces; ++i) {
        printf("%s: %dx%d+%d+%d", surfaces[i].name,
               surfaces[i].width, surfaces[i].height, surfaces[i].x, surfaces[i].y);

        if ((surfaces[i].mmwidth != -1) || (surfaces[i].mmheight != -1)) {
            printf(" (%dx%d)", surfaces[i].mmwidth, surfaces[i].mmheight);
        }

        if (surfaces[i].is_desktop == 0) {
            printf(" %s", surfaces[i].is_landscape?"landscape":"portrait");
        }

        putchar('\n');
    }
}

void surfaces_print_names()
{
    for (int i = 0; i < nsurfaces; ++i) {
        printf("%s\n", surfaces[i].name);
    }
}
