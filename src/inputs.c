/* Copyright © 2023 Rémi Hérilier
 * This file is part of the wacom-screen projet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "inputs.h"

#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <X11/Xatom.h>

#include <X11/extensions/XInput.h>

#include <xorg/libinput-properties.h>
#include <xorg/wacom-properties.h>
#include <xorg/Xwacom.h>

static int ninputs = 0;
static struct input_t* inputs = NULL;

static Atom atom_keyboard;
static Atom atom_mouse;
static Atom atom_dev_node;
static Atom atom_wacom_area;
static Atom atom_wacom_rotation;
static Atom atom_float;
static Atom atom_matrix_prop;

static int device_get_string(Display* dpy, XDevice* device, Atom prop, char** str)
{
    Atom type;
    unsigned char *data;
    unsigned long n, bytes_after;
    int ret, format;

    ret = XGetDeviceProperty(dpy, device, prop, 0, 1000, False,
                             XA_STRING, &type, &format, &n,
                             &bytes_after, &data);

    if (ret != Success) {
        return -1;
    }

    *str = strdup((char*)data);

    XFree(data);

    return 0;
}

static int device_get_ints(Display* dpy, XDevice* device, Atom prop, long* ints, unsigned long nints)
{
    Atom           type;
    unsigned long  nitems, bytes_after, i;
    int            ret, format;
    unsigned char* data;

    ret = XGetDeviceProperty(dpy, device, prop, 0, nints, False, XA_INTEGER,
                             &type, &format, &nitems, &bytes_after, &data);

    if (ret != Success) {
        return -1;
    }

    if (nitems != nints) {
        return -1;
    }

    switch (format) {
    case 8:
        for (i = 0; i < nitems; ++i) {
            ints[i] = data[i];
        }
        break;
    case 16:
        for (i = 0; i < nitems; ++i) {
            ints[i] = ((short*)data)[i];
        }
        break;
    case 32:
        for (i = 0; i < nitems; ++i) {
            ints[i] = ((long*)data)[i];
        }
        break;
    }

    XFree(data);

    return 1;
}

static int device_set_ints(Display* dpy, XDevice* device, Atom prop, const long* ints, unsigned long nints)
{
    Atom           type;
    unsigned long  nitems, bytes_after;
    int            ret, format;
    unsigned char* data;

    ret = XGetDeviceProperty(dpy, device, prop, 0, 1000, False, AnyPropertyType,
                             &type, &format, &nitems, &bytes_after, &data);

    if (ret != Success) {
        return -1;
    }

    if ((type != XA_INTEGER) || (format != 32) || (nitems != nints)) {
        printf("unexpected type for property %lu\n", prop);

        return -1;
    }

    XChangeDeviceProperty(dpy, device, prop, type, format, PropModeReplace, (const unsigned char*)ints, nitems);
    XFlush(dpy);

    return 0;
}

static int device_set_floats(Display* dpy, XDevice* device, Atom prop, const float* floats, unsigned long nfloats)
{
    Atom type;
    unsigned long nitems, bytes_after;
    int ret, format;
    unsigned long* data;

    ret = XGetDeviceProperty(dpy, device, prop, 0, nfloats, False, AnyPropertyType,
                             &type, &format, &nitems, &bytes_after, (unsigned char**)&data);

    if (ret != Success) {
        return -1;
    }

    if ((type != atom_float) || (format != 32) || (nitems != nfloats)) {
        printf("unexpected type for property %lu\n", prop);

        return -1;
    }

    for (unsigned long i = 0; i < nfloats; ++i) {
        // cppcheck-suppress invalidPointerCast
        *(float*)(&data[i]) = floats[i];
    }

    XChangeDeviceProperty(dpy, device, prop, type, format, PropModeReplace, (unsigned char*)data, nitems);
    XFlush(dpy);
    XFree(data);

    return 0;
}

int inputs_init(Display* dpy)
{
    const long reset_area[4] = { -1, -1, -1, -1 };

    XDeviceInfo* infos;
    int ninfos;
    int iinputs;

    atom_wacom_area = XInternAtom(dpy, WACOM_PROP_TABLET_AREA, True);

    if (atom_wacom_area == 0) {
        return ENODEV;
    }

    /* input devices
     */
    infos = XListInputDevices(dpy, &ninfos);

    atom_keyboard = XInternAtom(dpy, XI_KEYBOARD, True);
    atom_mouse    = XInternAtom(dpy, XI_MOUSE, True);
    atom_dev_node = XInternAtom(dpy, "Device Node", True);
    atom_float    = XInternAtom(dpy, "FLOAT", True);
    atom_matrix_prop = XInternAtom(dpy, "Coordinate Transformation Matrix", True);
    atom_wacom_rotation = XInternAtom(dpy, WACOM_PROP_ROTATION, True);

    for (int i = 0; i < ninfos; ++i) {
        XDeviceInfo info = infos[i];

        if ((info.type == 0)
            || (info.type == atom_keyboard)
            || (info.type == atom_mouse)) {
            /* ignoring virtual devices, as for keyboards and mouses */
            continue;
        }

        ++ninputs;
    }

    inputs = (struct input_t*)malloc(sizeof(struct input_t) * ninputs);
    iinputs = 0;

    for (int i = 0; i < ninfos; ++i) {
        XDeviceInfo info = infos[i];

        if ((info.type == 0)
            || (info.type == atom_keyboard)
            || (info.type == atom_mouse)) {
            /* ignoring virtual devices, as for keyboards and mouses */
            continue;
        }

        inputs[iinputs].name = strdup(info.name);
        inputs[iinputs].type = XGetAtomName(dpy, info.type);
        inputs[iinputs].id = info.id;

        XDevice* device = XOpenDevice(dpy, info.id);
        int nprops;
        Atom* props = XListDeviceProperties(dpy, device, &nprops);

        for (int j = 0; j < nprops; ++j) {
            if (props[j] == atom_dev_node) {
                device_get_string(dpy, device, atom_dev_node, &inputs[iinputs].dev);
            } else if (props[j] == atom_wacom_area) {
                /* an overkilled (but mandatory) mess to get the device default dimension...
                 */
                device_get_ints(dpy, device, atom_wacom_area, inputs[iinputs].current_area, 4);
                device_set_ints(dpy, device, atom_wacom_area, reset_area, 4);
                device_get_ints(dpy, device, atom_wacom_area, inputs[iinputs].default_area, 4);
                device_set_ints(dpy, device, atom_wacom_area, inputs[iinputs].current_area, 4);
            } else if (props[j] == atom_wacom_rotation) {
                long rotation;

                device_get_ints(dpy, device, atom_wacom_rotation, &rotation, 1);
                inputs[iinputs].is_landscape = ((rotation == ROTATE_NONE) || (rotation == ROTATE_HALF));
            }
        }

        XFree(props);
        XCloseDevice(dpy, device);

        ++iinputs;
    }

    XFreeDeviceList(infos);

    return 0;
}

const struct input_t* inputs_exists(const char* name)
{
    for (int i = 0; i < ninputs; ++i) {
        if (strcmp(name, inputs[i].name) == 0) {
            return inputs + i;
        }
    }

    return NULL;
}

void inputs_print_details()
{
    for (int i = 0; i < ninputs; ++i) {
        printf("%s:\n", inputs[i].name);
        printf("  type        : %s\n", inputs[i].type);
        printf("  dev         : %s\n", inputs[i].dev);
        printf("  id          : %lu\n", inputs[i].id);
        printf("  default area: %ld %ld %ld %ld\n",
               inputs[i].default_area[0], inputs[i].default_area[1],
               inputs[i].default_area[2], inputs[i].default_area[3]);
        printf("  current area: %ld %ld %ld %ld\n",
               inputs[i].current_area[0], inputs[i].current_area[1],
               inputs[i].current_area[2], inputs[i].current_area[3]);
        printf("  orientation : %s\n", inputs[i].is_landscape?"landscape":"portrait");
    }
}

void inputs_print_names()
{
    for (int i = 0; i < ninputs; ++i) {
        printf("%s\n", inputs[i].name);
    }
}

void inputs_set_area(Display* dpy, const struct input_t* input, const float aspect_ratio)
{
    // can be interpreted as [X, Y, W, H]
    const long* default_area = input->default_area;
    const float dar = default_area[2] / (float)default_area[3];

    if (dar == aspect_ratio) {
        return;
    }

    // has to be considered as [X0, Y0, X1, Y1]
    long new_area[4];

    float cx = default_area[2] / 200.0f;
    float cy = default_area[3] / 200.0f;
    float hw, hh;

    if (dar < aspect_ratio) {
        hw = cx;
        hh = cx / aspect_ratio;
    } else {
        hw = cx * aspect_ratio;
        hh = cy;
    }

    new_area[0] = 0.5f + (cx - hw) * 100; // X0
    new_area[1] = 0.5f + (cy - hh) * 100; // Y0
    new_area[2] = 0.5f + (cx + hw) * 100; // X1
    new_area[3] = 0.5f + (cy + hh) * 100; // Y1

    for (int i = 0; i < ninputs; ++i) {
        if (strcmp(input->dev, inputs[i].dev) == 0) {
            XDevice* dev = XOpenDevice(dpy, inputs[i].id);

            device_set_ints(dpy, dev, atom_wacom_area, new_area, 4);
            XCloseDevice(dpy, dev);
        }
    }
}

void inputs_reset_area(Display* dpy, const struct input_t* input)
{
    const long area[] = { -1, -1, -1, -1 };

    for (int i = 0; i < ninputs; ++i) {
        if (strcmp(input->dev, inputs[i].dev) == 0) {
            XDevice* dev = XOpenDevice(dpy, inputs[i].id);

            device_set_ints(dpy, dev, atom_wacom_area, area, 4);
            XCloseDevice(dpy, dev);
        }
    }
}

void inputs_set_viewport(Display* dpy, const struct input_t* input, int x, int y, int w, int h)
{
    float matrix[] = {
        1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 1.0f
    };

    float sw = (float)DisplayWidth(dpy, DefaultScreen(dpy));;
    float sh = (float)DisplayHeight(dpy, DefaultScreen(dpy));

    // scaling
    matrix[0] = w / sw;
    matrix[4] = h / sh;
    // translation
    matrix[2] = x / sw;
    matrix[5] = y / sh;

    for (int i = 0; i < ninputs; ++i) {
        if (strcmp(input->dev, inputs[i].dev) == 0) {
            XDevice* dev = XOpenDevice(dpy, inputs[i].id);

            device_set_floats(dpy, dev, atom_matrix_prop, matrix, 9);
            XCloseDevice(dpy, dev);
        }
    }
}
